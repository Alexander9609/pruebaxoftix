# -*- coding: utf-8 -*-
"""
Created on Sat Aug 10 09:05:23 2019

@author: jorge
"""

from flask import Flask, render_template, request, redirect, url_for
#import Conexionbd as conexion
import pymysql

db = pymysql.connect("localhost","root","","asienda")
##################################################

# prepare a cursor object using cursor() method
cursor = db.cursor()


app = Flask(__name__)

@app.route('/')
def home():
    mycursor = db.cursor()

    mycursor.execute("SELECT * FROM maquina")

    data = mycursor.fetchall()
   # print (data)
    mycursor.execute("SELECT m.pre_ho_maquina FROM maquina m WHERE m.tipo_maquina LIKE '%nueva'")
    dataconsul = mycursor.fetchall()
    
    
    mycursor.execute("SELECT f.nom_finca from finca f INNER JOIN parte p on f.id_finca=p.finca_id_finca INNER JOIN maquina m ON m.id_maquina=p.maquina_id_maquina where p.tiempo_parte>=5 AND m.pre_ho_maquina>=20000")
    dataconsul2 = mycursor.fetchall()
    print (dataconsul2)
    return render_template('home.html', listmaquinarias=data, lisval=dataconsul, lisfinca=dataconsul2)

#SELECT f.nom_finca from finca f INNER JOIN parte p on f.id_finca=p.finca_id_finca INNER JOIN maquina m ON m.id_maquina=p.maquina_id_maquina where p.tiempo_parte>=5 AND m.pre_ho_maquina>=20000
    
#SELECT m.pre_ho_maquina FROM maquina m WHERE m.tipo_maquina LIKE '%nueva'


@app.route('/maquinaria', methods=['POST'])
def ReMaquinaria():
    if request.method=='POST':
        tipo=request.form['tipoma']
        matricula=request.form['matriculama']
        precio=request.form['precio']
        print (tipo, matricula,precio)
        sql = "INSERT INTO maquina(id_maquina, tipo_maquina, matricula_maquina,pre_ho_maquina) \
        VALUES (NULL,'{0}','{1}','{2}')".format(tipo,matricula,precio)
        try:
           cursor.execute(sql)
           db.commit()
           return redirect(url_for('home'))
        except:
           db.rollback()
           return redirect(url_for('home'))
        

@app.route('/finca', methods=['POST'])
def ReFinca():
    if request.method=='POST':
        nomfinca=request.form['nomfinca']
        extenmaquina=request.form['extenmaquina']
        
        print (nomfinca, extenmaquina)
        
        sql = "INSERT INTO finca(id_finca, nom_finca, exten_finca) \
        VALUES (NULL,'{0}','{1}')".format(nomfinca,extenmaquina,)
        try:
           cursor.execute(sql)
           db.commit()
           return redirect(url_for('home'))
        except:
           db.rollback()
           return redirect(url_for('home'))
           
    return "exitoso"

@app.route('/trabajador', methods=['POST'])
def Retrabajador():
    if request.method=='POST':
        nomtrabjador=request.form['nomtrabjador']
        dirtrabajador=request.form['dirtrabajador']
        
        print (nomtrabjador, dirtrabajador)
        
        sql = "INSERT INTO trabajador(id_trabajador, nom_trabajador, dir_trabajador) \
        VALUES (NULL,'{0}','{1}')".format(nomtrabjador,dirtrabajador)
        try:
           cursor.execute(sql)
           db.commit()
           return redirect(url_for('home'))
        except:
           db.rollback()
           return redirect(url_for('home'))
           
    return "exitoso"

@app.route('/deleteuser')
def deletecontac():
    return  render_template('delete.html')

@app.route('/edituser')
def edit():
    return  render_template('edit.html')


#print (conexion.coneccionbd())


##inicia el servidor

if __name__=='__main__':
    app.run(debug=True, use_reloader=False)
